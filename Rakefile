require 'rake'
require 'fileutils'

desc "Hook our dotfiles into system-standard positions."
task :install do
  puts
  puts "======================================================"
  puts "Welcome to TW Installation."
  puts "======================================================"
  puts
  request_permission
  install_homebrew if RUBY_PLATFORM.downcase.include?("darwin")
  adding_taps
  update_brew_formulae
   
  if want_to_install?('OSX Core Tools (highly recommended)')
    Rake::Task["install_core_tools"].execute
  end

  if want_to_install?('OSX Core Applications (highly recommended)')
    Rake::Task["install_core_applications"].execute
  end

  if want_to_install?('File Storage Applications (not recommended for work)')
    Rake::Task["install_file_storage_applications"].execute
  end

  if want_to_install?('Entertainment Applications (not recommended for work)')
    Rake::Task["install_entertainment_applications"].execute
  end

  if want_to_install?('Gaming Applications (not recommended for work)')
    Rake::Task["install_gaming_applications"].execute
  end

  if want_to_install?('Development Tools (highly recommended)')
    Rake::Task["install_dev_tools"].execute
  end

  if want_to_install?('Development Applications (highly recommended)')
    Rake::Task["install_dev_applications"].execute
  end

  if want_to_install?('Ruby Development Environment (highly recommended)')
    Rake::Task["install_ruby_environment"].execute
  end

  if want_to_install?('Java Development Environment')
    Rake::Task["install_java_environment"].execute
  end

  brew_cleanup
  brew_linkapps

  success_msg("installed")
end

desc "Installs OSX"
task :install_core_tools do 
  puts "====================================================="
  puts "Installing OSX utilities."
  puts "====================================================="

  formulae = [
    "coreutils",
    "moreutils",
    "gnu-sed --with-default-names",
    "wget --with-iri",
    "homebrew/dupes/grep",
    "homebrew/dupes/openssh",
    "homebrew/dupes/screen",
    "bash",
    "mackup"
  ]
  formulae.each do |formula|
    run %{brew install #{formula}}
  end

  puts
  puts
end

desc "Installs core OSX applications (highly recommended)"
task :install_core_applications do
  puts "======================================================"
  puts "Installing core applications for OSX."
  puts "======================================================"

  applications = [
    'hyperdock',
    'spectacle',
    'alfred',
    'google-chrome',
    'skype',
    'google-hangouts',
    'evernote',
    'slack',
    'hipchat',
    'seil',
    'qlstephen',
    'the-unarchiver',
    'unetbootin',
    'flux',
    'transmit',
    'kindle',
    'beardedspice',
    'aerial',
    'bartender',
    'appcleaner',
    'helium',
    'nordvpn'
  ]
  applications.each do |application|
    brew_cask_install(application)
  end

  puts
  puts
end

desc "Installs file storage applications"
task :install_file_storage_applications do
  puts "======================================================"
  puts "Installing File Storage Applications."
  puts "======================================================"

  applications = [
    'dropbox',
    'google-drive'
  ]
  applications.each do |application|
    brew_cask_install(application)
  end
  
  puts
  puts
end

desc "Installs entertainment applications (not recommended for work)"
task :install_entertainment_applications do
  puts "======================================================"
  puts "Installing Entertainment Applications."
  puts "======================================================"

  applications = [
    'soundcleod',
    'vlc',
    'deluge',
    'music-manager',
    'subtitles',
    'plex-media-server',
    'google-plus-auto-backup',
    'yakyak',
    'whatsapp',
    'jadengeller-helium',
    'contexts',
    'hazel',
    'istats-menu'
  ]
  applications.each do |application|
    brew_cask_install(application)
  end
  
  puts
  puts
end

desc "Installs gaming applications (not recommended for work)"
task :install_gaming_applications do
  puts "======================================================"
  puts "Installing Gaming Applications."
  puts "======================================================"
  
  applications = [
    'steam',
    'logitech-gaming-software',
    'gfxcardstatus'
  ]
  applications.each do |application| 
    brew_cask_install(application)
  end

  puts
  puts
end

desc "Installs development tools"
task :install_dev_tools do
  puts "======================================================"
  puts "Installing base development tools"
  puts "======================================================"
  
  tools = [
  'mongodb',
  'go',
  'heroku-toolbelt',
  'postgres',
  'gpm',
  'pyenv',
  'python',
  'tmux',
  'nvm',
  'fzf',
  'git',
  'gitflow',
  'ccmenu'
  ]
  tools.each do |tool|
    run %{brew install #{tool}}
  end
  
  puts
  puts
end

desc "Installs development applications"
task :install_dev_applications do
  puts "======================================================"
  puts "Installing developer applications."
  puts "======================================================"

  applications = [
    'paw',
    'charles',
    'webstorm',
    'firefox',
    'iterm2',
    'parallels-desktop',
    'sketch',
    'kaleidoscope',
    'dash',
    'tower',
    'datagrip',
    'marked',
    'zeplin',
    'invisionsync',
    'go2shell',
    'gitup'
  ]
  applications.each do |application|
    brew_cask_install(application)
  end

  puts
  puts
end

desc "Installs java development environment"
task :install_java_environment do
  puts "======================================================"
  puts "Installing java environment"
  puts "======================================================"

  # Java Casks 
  casks = [
    'java'
  ]
  casks.each do |cask|
    brew_cask_install(cask)
  end
end

desc "Installs ruby development environment"
task :install_ruby_environment do
  puts "======================================================"
  puts "Installing ruby environment."
  puts "======================================================"

  # Installs RVM
  run %{which rvm}
  unless $?.success?
    run %{\\curl -sSL https://get.rvm.io | bash -s stable}
  end

  # Sourcing rvm
  run %{source ~/.rvm/scripts/rvm}

  # Install ruby
  run %{rvm install 2.2.3}

  # Set default ruby
  run %{rvm --default use 2.2.3}

  gems = [
    'lunchy',
    'sass',
    'compass',
    'cocoapods'
  ]
  gems.each do |gem|
    run %{gem install #{gem}}
  end

  puts
  puts
end

task :default => 'install'


private
def run(cmd)
  puts "[Running] #{cmd}"
  `#{cmd}` unless ENV['DEBUG']
end
def request_permission
  run %{sudo -v}
end
def brew_cask_install(application)
  run %{brew cask install --appdir="/Applications" #{application}}
end
def brew_cleanup
  run %{brew cleanup}
end
def update_brew_formulae
  run %{brew upgrade -all}
end
def brew_linkapps
  run %{brew linkapps}
end
def number_of_cores
  if RUBY_PLATFORM.downcase.include?("darwin")
    cores = run %{ sysctl -n hw.ncpu }
  else
    cores = run %{ nproc }
  end
  puts
  cores.to_i
end

def install_homebrew
  run %{which brew}
  unless $?.success?
    puts "======================================================"
    puts "Installing Homebrew, the OSX package manager...If it's"
    puts "already installed, this will do nothing."
    puts "======================================================"
    run %{ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"}
  end

  puts
  puts
  puts "======================================================"
  puts "Updating Homebrew."
  puts "======================================================"
  run %{brew update}
  puts
  puts
end

def adding_taps
  puts "======================================================="
  puts "Adding Taps."
  puts "======================================================="
  taps = [
    "caskroom/cask",
    "homebrew/dupes",
    "homebrew/versions",
    "bramstein/webfonttools",
    "caskroom/fonts"
  ]
  taps.each do |tap|
    run %{brew tap #{tap}}
  end
  puts
  puts

end

def ask(message, values)
  puts message
  while true
    values.each_with_index { |val, idx| puts " #{idx+1}. #{val}" }
    selection = STDIN.gets.chomp
    if (Float(selection)==nil rescue true) || selection.to_i < 0 || selection.to_i > values.size+1
      puts "ERROR: Invalid selection.\n\n"
    else
      break
    end
  end
  selection = selection.to_i-1
  values[selection]
end

def want_to_install? (section)
  if ENV["ASK"]=="true"
    puts "Would you like to install configuration files for: #{section}? [y]es, [n]o"
    STDIN.gets.chomp == 'y'
  else
    true
  end
end

def install_files(files, method = :symlink)
  files.each do |f|
    file = f.split('/').last
    source = "#{ENV["PWD"]}/#{f}"
    target = "#{ENV["HOME"]}/.#{file}"

    puts "======================#{file}=============================="
    puts "Source: #{source}"
    puts "Target: #{target}"

    if File.exists?(target) && (!File.symlink?(target) || (File.symlink?(target) && File.readlink(target) != source))
      puts "[Overwriting] #{target}...leaving original at #{target}.backup..."
      run %{ mv "$HOME/.#{file}" "$HOME/.#{file}.backup" }
    end

    if method == :symlink
      run %{ ln -nfs "#{source}" "#{target}" }
    else
      run %{ cp -f "#{source}" "#{target}" }
    end

    # Temporary solution until we find a way to allow customization
    # This modifies zshrc to load all of yadr's zsh extensions.
    # Eventually yadr's zsh extensions should be ported to prezto modules.
    if file == 'zshrc'
      File.open(target, 'a') do |zshrc|
        zshrc.puts('for config_file ($HOME/.yadr/zsh/*.zsh) source $config_file')
      end
    end

    puts "=========================================================="
    puts
  end
end

def success_msg(action)
  puts ""
  puts "OSX setup has been #{action}." 
end
